import { RouteRecordRaw } from 'vue-router'

const CatRoutes: RouteRecordRaw = {
  path: '/cats',

  component: () => import('layouts/MainLayout.vue'),

  children: [
    {
      path: '',
      component: () => import('pages/cats/IndexPage.vue'),
      name: 'CatsIndex'
    }
  ]
}

export default CatRoutes
